console.log("Hola Mundo!");

// Carga la configuración por defecto, busca un fichero .env, y carga en memoria las variables
require('dotenv').config();

//Carga de libreria express
const express = require('express');
// Se crea la aplicacion
const app = express();
// Para que se interprete el body como JSON
app.use(express.json());

const io = require('./io');

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');

const port = process.env.PORT || 3000;
// Iniciar la escucha en el servidor
app.listen(port);

console.log("API escuchando en el puerto BIP BIP " + port);

// Ruta a la que hay q hacer la petición /apitechu/v1/hello
// el segundo parametro es el handler, q normalmente tiene 2 parametros
// REQ q tiene unas cabeceras (HEADERS), de este modo nos llega el objeto ya
// parseado
app.get("/apitechu/v1/hello",
  function(req, res) {
    console.log("GET /apitechu/v1/hello");

    // Si no se especifica nada, se asume q la respuesta es formato HTML
//    res.send("Hola desde API TechU!");
    // Con esto enviamos la respuesta en formato JSON
    // PostMan inicialmente lo interpreta como HTML, pero se puede cambiar a JSON
//    res.send('{"msg" : "Hola desde API TechU!"}');
    // Se puede enviar sin las comillas simples, y tb se interpreta como JSON
    // Con esto, directamente en PostMan lo interpreta como JSON
    res.send({"msg" : "Hola desde API TechU!"});
  }
)

// No es necesario pasar explícitamente los parámetros req y res
app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

/* PRACTICA POST LOGIN*/
app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v2/login", authController.loginV2);
/* PRACTICA POST LOGOUT*/
app.post("/apitechu/v1/logout", authController.logoutV1);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)
