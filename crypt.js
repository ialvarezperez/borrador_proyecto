const bcrypt = require('bcrypt');

function hash(data) {
  console.log("Hashing data");

  // También existe la función asíncrona
  // El segundo parámetro es el número de pasadas.
  return bcrypt.hashSync(data, 10);
}

function checkpassword(sentPassword, userHashPassword){
  console.log("Checking password");

  // Esto devuelve un booleano
  return bcrypt.compareSync(sentPassword, userHashPassword);
}

module.exports.hash = hash;
module.exports.checkpassword = checkpassword;
