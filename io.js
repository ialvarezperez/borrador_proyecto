const fs = require('fs');

// Las transformaciones de datos las hacemos en esta función
function writeUserDataToFile(data){

  var jsonUserData = JSON.stringify(data);

//    fs.writeFile("./usuarios-2.json", jsonUserData, "utf8",
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    // Si la función writeFile al finalizar ha habido error, pobla la variable err
    // Esto es un callback
    function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en el fichero");
      }
    }
  );

}

// Se exporta la función, para que pueda ser utilizada desde otro fichero.
module.exports.writeUserDataToFile = writeUserDataToFile;
