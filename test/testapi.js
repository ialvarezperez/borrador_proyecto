const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server');

// El punto . indica q estamos encadenando métodos
// describe es una suite
describe('First test',
  function() {
    // it es cada test de la suite
    it('Test that Google works', function(done) {
      // chai.request te permite encadenar metodos, a nivel interno se devuelve un puntero a si mismo, un this
      chai.request('http://www.google.com') //Devuelve 200
//      chai.request('https://developer.mozilla.org/en-US/adsfasdfas') // Devuelve 400
        .get('/')
        .end( // Lo que se ejecuta cuando finaliza cuando finaliza
          function(err, res) {
            console.log("Request finished");
            console.log(err);
            //console.log(res);

            res.should.have.status(200);
            done(); // Es como return
          }
        )
      }
    )
  }
)

describe('Test de API de Usuarios',
  function() {
    it('Prueba que la API de usuarios responde', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU!");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de de usuarios correcta', function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            console.log("Request finished");
            res.should.have.status(200);

            for (user of res.body.list) {
              user.should.have.property("email");
              user.should.have.property("password");
            }

            done();
          }
        )
      }
    )
  }
)
