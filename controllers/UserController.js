const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8ediap/collections/";

// Para obtener status 500 de una petición get
//const mlabBaseURL = "https://aapi.mlab.com/api/1/databases/apitechu8ediap/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

var userId;

function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");
  console.log(req.query);

  //el segundo parametro es para indicar la ruta donde buscar el fichero
  // con la constante __dirname nos abstraemos de SO windown, linux...
  // es una constante de node
//res.sendFile('usuarios.json', {root: __dirname});

  // users es una variable tipo array
  var users = require('../usuarios.json');
//res.send(users);

  var result = {};

  if (req.query.$count == 'true'){

    result.count = users.length;

  }

  // result.list = (req.query.$top)? users.slice(0, req.query.$top) : users;

  // Si el valor del parametro top no es numerico, se devuelve toda la lista
  result.list = ((req.query.$top) && !(isNaN(req.query.$top)))?
    users.slice(0, req.query.$top) : users;

  res.send(result);

}

function getUsersV2 (req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + mlabAPIKey,
    function(err, resMLab, body) {
      var response = !err ?
        body : {"msg" : "Error obteniendo usuarios"}

      res.send(response);
    }
  )
}

function getUserByIdV2 (req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        // Se establece el status, pero no se envía hasta q se hace el send.
        // O se puede enviar concatenado res.status(500).send()
        res.status(500);
      } else {
        if (body.length > 0){
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }

/*
      var response = !err ?
        body : {"msg" : "Error obteniendo usuario"}
*/
      res.send(response);
    }
  )
}

function createUserV1 (req, res) {
  console.log("POST /apitechu/v1/users");

  // req es un objeto q monta express
//    console.log(req.headers);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  };

  var users = require("../usuarios.json");
  users.push(newUser);

  io.writeUserDataToFile(users);

  res.send("Usuario añadido con éxito");
}

function createUserV2 (req, res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  httpClient.post("user?" + mlabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario guardado con éxito");
      res.status(201);
      res.send({"msg" : "Usuario creado con éxito"});
    }
  )
}

function findUser(user){
  console.log(userId + ' ' + user.id);

  return user.id == userId;
}

function deleteUserV1 (req, res) {
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("La id enviada es: " + req.params.id);

  var users = require('../usuarios.json');
  //    users.splice(req.params.id - 1, 1);

  const success = 'Usuario borrado con éxito';
  const fail = 'Usuario no encontrado';
  var found = fail;

  // FOR
/*
  for (var i = 0; i < users.length; i++) {
    console.log(req.params.id + ' ' + users[i].id);
    if (users[i].id  == req.params.id){
        users.splice(i, 1);
        found = success;
        break;
    }
  }
*/

// FOR IN
/*
for (var i in users) {
  console.log(req.params.id + ' ' + users[i].id);
  if(users[i].id == req.params.id){
    users.splice(i, 1);
    found = success;
    break;
  }
}
*/

// FOR OF
/*
var i = 0;

for (var user of users) {
  console.log(req.params.id + ' ' + user.id);
  if (user.id == req.params.id){
    users.splice(i, 1);
    found = success;
    break;
  }
  i++;
}
*/

// ARRAY FOREACH (no permite break)
/*
users.forEach(function(element, index){
  console.log(req.params.id + ' ' + element.id);
  if (element.id == req.params.id){
    users.splice(index, 1);
    found = success;
  }

}
);
*/

// ARRAY FIND INDEX (no tiene sentido break)
// Opción 1
/*
userId = req.params.id;

users.splice(users.findIndex(findUser), 1);
*/

  // Opción 2

var index = users.findIndex(function(element){
  console.log(req.params.id + ' ' + element.id);
  return element.id == req.params.id;

});

if (index >= 0){

  users.splice(index, 1);
  found = success;

}


  io.writeUserDataToFile(users);
//    console.log("Usuario borrado");
  res.send({"msg" : found});

  console.log(found);

}

// Se exporta la función, para q esté disponible al exterior
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
