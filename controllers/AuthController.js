const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8ediap/collections/";

// Para obtener status 500 de una petición get
//const mlabBaseURL = "https://aapi.mlab.com/api/1/databases/apitechu8ediap/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1 (req, res) {
  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var logged = false;
  var usersRes = {};

  var users = require("../usuarios.json");

  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.email + " y " +   req.body.email);
      return element.email == req.body.email
    }
  )

  console.log("indexOfElement es " + indexOfElement);

  if (indexOfElement >= 0){
    // Usuario encontrado, comprobamos password
    console.log("comparando " + users[indexOfElement].password + " y " +   req.body.password);
    if (users[indexOfElement].password == req.body.password){

      // Login OK
      logged = true;
      users[indexOfElement].logged = true;
      // Cuando extraemos la funcion writeUserDataToFile() a un fichero JS aparte, para poder invocar la function
      // incluimos por delante de la llamada io.
      io.writeUserDataToFile(users);

    }
  }

  if (logged){
    usersRes.mensaje = 'Login correcto';
    usersRes.idUsuario = users[indexOfElement].id;
  }
  else{
    usersRes.mensaje = 'Login incorrecto';
  }

  console.log(usersRes.mensaje);
  res.send(usersRes);

}

function loginV2 (req, res) {
  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  var query = 'q={"email": "' + req.body.email + '" }';
  console.log("query: " + query);

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err){
        res.status(500);
        res.send({"msg" : "Error obteniendo usuario"});
      } else {
        if (body.length == 0){
          res.status(401);
          res.send({"msg" : "Login incorrecto, email y/o password no encontrados"});
        } else {
          var user = body[0];
          if (!crypt.checkpassword(req.body.password, user.password)){
            res.status(401);
            res.send({"msg" : "Login incorrecto, email y/o password no encontrados"});
          } else {
            console.log("Email found, password ok");
            var putBody = '{"$set":{"logged":true}}';
            console.log("Logged in user with id " + user.id);

            httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT) {
                if (errPUT){
                  res.status(500);
                  res.send({"msg" : "Error realizando login"});
                } else {
                  console.log("Usuario actualizado con éxito");
                  var response = {
                    "msg" : "Login correcto",
                    "idUsuario" : user.id
                  }
                  res.status(201);
                  res.send(response);
                }
              }
            )
          }
        }
      }
    }
  )
}

function logoutV1 (req, res) {
  console.log("POST /apitechu/v1/logout");
  console.log(req.body.id);

  var logged_out = false;
  var usersRes = {};

  var users = require("../usuarios.json");

  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   req.body.id);
      return element.id == req.body.id
    }
  )

  console.log("indexOfElement es " + indexOfElement);

  if (indexOfElement >= 0){
    // Usuario encontrado, comprobamos si está logado
    console.log("logged " + users[indexOfElement].logged);
    if (users[indexOfElement].logged){
      // Logout correcto
      logged_out = true;
      delete users[indexOfElement].logged;
      io.writeUserDataToFile(users);

    }
  }

  if (logged_out){
    usersRes.mensaje = 'logout correcto';
    usersRes.idUsuario = users[indexOfElement].id;
  }
  else{
    usersRes.mensaje = 'logout incorrecto';
  }

  console.log(usersRes.mensaje);
  res.send(usersRes);

}

function logoutV2 (req, res) {
  console.log("POST /apitechu/v2/logout");
  console.log(req.params.id);

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("Client created");

  var query = 'q={"id": ' + req.params.id + ' , "logged":true }';
  var putBody = '{"$unset":{"logged":""}}';

  httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Logout incorrecto"
        }
        res.status(500);
      } else {
        if (body.n == 0){
          var response = {
            "msg" : "Logout incorrecto"
          }
          res.status(404);
        } else {
          console.log("User found, loggin out");
          console.log("Logged out user with id " + req.params.id);
          var response = {
            "msg" : "Logout correcto"
          }
          res.status(200);
        }
      }
      response.idUsuario = req.params.id;
      res.send(response);
    }
  )
}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
