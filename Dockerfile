# Dockerfile apitechu

# Imagen raíz

FROM node

# Carpeta trabajo
# Carpeta donde se van a ejecutar los comandos CMD
WORKDIR /apitechu

# Se incluyen los ficheros de este directorio (ponemos .), en el directorio /apitechu de la imagen
# Todo el código que tenemos en borrador_proyecto, se van a copiar en el directorio /apitechu de la imagen
# Por ejemplo, en el contenedor ubuntu, tiene las carpetas de un SO ubuntu. Una vez creado, si se hace ls, se pueden ver esa
# carpetas
ADD . /apitechu

# Instalo los paquetes necesarios
# Se ejecuta en el proceso de creación de la imagen
RUN npm install

# Abrir puerto de la API
# Exponemos el puerto al exterior
EXPOSE 3000

# Comando de inicialización
# Se ejecuta al crear el contenedor
CMD ["npm", "start"]
